from config import Configuration
import logging
import sys
import math
import uuid
from datacenter import Datacenter
from stack import Stack
from ruamel.yaml.compat import StringIO as ruamelStringIO
from io import StringIO
from bottle import run, response, post, request, delete
from ruamel.yaml import YAML  # To create YAML File and Read YAML File
from pathlib import Path
from translator.shell import TranslatorShell as TOSCATranslatorShell
from exception import WrongBlockDeviceMappingFormat, NoHostFoundException


def find_flavor(db, datacenter, tag):
    """
    This function try to find a flavor taking a list of flavors associated to the tag parameter and call
    Ecrae_ranking function to get
    :param db: database connector
    :param datacenter: Datacenter class instance
    :param tag: string
    :return: Flavor
    """
    # Find all flavors from tag
    flavors = db.getWhere('Tag', 'flavor', 'tag=\'' + tag + '\'')
    print(str(flavors));
    ranking = []
    #datacenter.print_flavors()
    for flavor in flavors:
        flavor_data = datacenter.get_flavor(flavor[0])
        # print(str(flavor_data))
        #in flavor data è contenuto anche l'host_id a cui il flavor fa riferimento, is_suitable confronta se il flavor "riesce a stare" nell'host
        host = datacenter.get_host(flavor_data.host_id)
        if host.is_suitable(flavor_data):
            ranking.append(ecrae_ranking(datacenter, flavor_data))
    if not ranking:
        raise NoHostFoundException('No suitable host found! Check if resources are available')
    best_choice = min(ranking, key=lambda x: x['score'])
    # Load flavor requirements to host
    best_choice['host'].add_flavor(best_choice['flavor'])
    return best_choice['flavor']

def ecrae_ranking(datacenter, flavor):
    """
    This function applies the Ecrae algorithm to calculate a score of an host
    :param datacenter: Datacenter
    :param flavor: Flavor
    :return: {Flavor, Host, Number}
    """
    host_id = flavor.host_id
    CPU_flavor = flavor.CPU
    host = datacenter.get_host(host_id)
    used_CPU = host.used_CPU
    total_CPU = host.CPU
    power_array = host.power_array.split(', ')
    used_power_consumption_index = math.ceil((used_CPU / total_CPU) * 10)
    used_power_consumption = int(power_array[used_power_consumption_index])
    increment = CPU_flavor / total_CPU
    score = increment * used_power_consumption
    print('Host : ', host_id, 'score: ', score)
    return {"flavor": flavor, "host": host, "score": score}

def deploy_stack(conf, db, datacenter, heat):
    """
    This function parses the TOSCA template and translates it to an Heat template.
    :param conf: Configuration
    :param db: database connector
    :param datacenter: Datacenter
    :param heat: heat client
    :return stack_id: String
    """
    instances = []
    template = request.body.read()  ## Read the Yaml File
    yaml = YAML()  ## Load the yaml object
    # yaml.preserve_quotes = True
    tosca_template = yaml.load(template)  # Load content of YAML file to yaml object
    tosca_template_copy = yaml.load(template)
    # To avoid OrderedDict RuntimeException iterate on tosca_template and modify tosca_template_copy
    # print(tosca_template)
    user_data_script_dict = {}
    for node_type in tosca_template['node_types']:
        # print(node_type)
        for item_type in tosca_template['node_types'][node_type]:
            if item_type == 'properties':
                for type_property in tosca_template['node_types'][node_type]['properties']:
                    # print(type_property)
                    if type_property == 'tag':
                        del tosca_template_copy['node_types'][node_type]['properties'][type_property]
                        tosca_template_copy['node_types'][node_type]['properties']['flavor'] = {'type': 'string'}

    nodes = tosca_template['topology_template']['node_templates']
    for node in nodes:
        # print(node)
        for field in tosca_template['topology_template']['node_templates'][node]:
            # print(field)
            if field == 'properties':
                for node_property in tosca_template['topology_template']['node_templates'][node][field]:
                    if node_property == 'tag':
                        tag = tosca_template['topology_template']['node_templates'][node][field][node_property]
                        print("tag: ", tag)
                        try:
                            instance = find_flavor(db, datacenter, tag)
                        except NoHostFoundException as e:
                            # deallocate other stack instance resources
                            for instance in instances:
                                host = datacenter.get_host(instance.host_id)
                                host.delete_flavor(instance)
                                raise NoHostFoundException('No suitable host found! Check if resources are available')
                        flavor_name = instance.name
                        instances.append(instance)
                        del tosca_template_copy['topology_template']['node_templates'][node][field][node_property]
                        tosca_template_copy['topology_template']['node_templates'][node][field][
                            'flavor'] = flavor_name
                    if node_property == 'user_data':
                        user_data_script_dict[
                            tosca_template['topology_template']['node_templates'][node][field]['name']] = \
                            yaml.load(tosca_template_copy['topology_template']['node_templates'][node][field][
                                          node_property])
    out_file = Path('afterEcrae.yaml')
    # print(out_file)
    output_to_file = open(str(out_file), "w")
    yaml.dump(tosca_template_copy, output_to_file)
    output_to_file.close()
    # redirect output of translator_shell
    old_stdout = sys.stdout
    sys.stdout = mystdout = StringIO()
    translator_shell = TOSCATranslatorShell()
    translator_shell.main(['--template-file=afterEcrae.yaml', '--template-type=tosca'])
    sys.stdout = old_stdout

    translated_template = mystdout.getvalue()
    stream_data = ruamelStringIO()
    heat_template = yaml.load(translated_template)
    heat_nodes = heat_template['resources']
    for node in heat_nodes:
        if 'user_data' in heat_template['resources'][node]['properties']:
            heat_template['resources'][node]['properties']['user_data'] = user_data_script_dict[node]
        if 'block_device_mapping' in heat_template['resources'][node]['properties']:
            volume_resource = ''
            for feature in heat_template['resources'][node]['properties']['block_device_mapping']:
                if 'volume_id' in feature:
                    volume_resource = feature['volume_id']['get_resource']
            if volume_resource == '':
                raise WrongBlockDeviceMappingFormat("volume_id not found in block_device_mapping!")
            heat_template['resources'][node]['properties']['block_device_mapping'] = [
                {"device_name": "vda", "volume_id": {"get_resource": volume_resource},
                 "delete_on_termination": "true"}]
    yaml.dump(heat_template, stream_data)

    """
        Call heat client to deploy translated template
    """
    heat_template['heat_template_version'] = str(heat_template['heat_template_version'])
    logging.debug(stream_data.getvalue())

    if conf.DEBUG_MODE == 'false':
        # TODO: add catch for heat exception that enable to restore host situation if heat fails
        returned_body = heat.stacks.create(stack_name="provaEcrae", template=heat_template)
        print(returned_body['stack']['id'])
        datacenter.add_stack(Stack(returned_body['stack']['id'], instances))
        logging.debug(stream_data.getvalue())
        return returned_body['stack']['id']
    else:
        # TODO: create and store stack object
        stack_id = 'debug_' + str(uuid.uuid4())
        datacenter.add_stack(Stack(stack_id, instances))
        return stack_id

def undeploy_stack(conf, datacenter, heat, stack_id):
    """
    This function deletes a stack instance
    :param conf: Configuration
    :param datacenter: Datacenter
    :param heat: heat client
    :param stack_id: String
    :return:
    """

    if conf.DEBUG_MODE is False:
        datacenter.delete_stack(stack_id)
        heat.stacks.delete(id)
    else:
        datacenter.delete_stack(stack_id)

def print_snapshot(datacenter):
    """
    Print all data about data center
    """
    print('_______________________________________________________')
    datacenter.print_flavors()
    print('_______________________________________________________')
    datacenter.print_hosts()