import logging
from host import Host
from flavor import Flavor
from stack import Stack
from exception import NoStackFoundException


class Datacenter:
    """
    Datacenter class represents the datacenter status in the orchestrator.
    """

    def __init__(self, db):
        """
        Init function loads every resources present in the db
        """
        self.hosts = []
        self.flavors = []
        self.stacks = []
        self.db = db

        # TODO: for now flavors and host are charged at init phase, add insert functions
        for host in self.db.get('Host', '*'):
            # db, host, CPU, RAM, storage, actual_CPU, actual_RAM, actual_storage, power_array
            self.hosts.append(Host(db, host[0], host[1], host[2], host[3], host[4], host[5], host[6], host[7]))
        for flavor in self.db.get('Flavor', '*'):
            self.flavors.append(Flavor(flavor[0], flavor[1], flavor[2], flavor[3], flavor[4]))
        for stack in self.db.get('Stack','*'):
            stack_id = stack[0]
            flavors = []
            for flavor_name in stack[1].split(','):
                print(flavor_name)
                flavor = self.get_flavor(flavor_name)
                flavors.append(flavor)
            self.stacks.append(Stack(stack_id, flavors))

    # TODO: add catch of index error exception
    def get_host(self, host_id):
        """
        Find the host with host_id and return it
        :param host_id: String
        :return: Host
        """
        return [item for item in self.hosts if item.host_id == host_id][0]

    def get_flavor(self, flavor_name):
        """
        Find flavor with flavor_name and return it
        :param flavor_name: String
        :return: Flavor
        """
        return [item for item in self.flavors if item.name == flavor_name][0]

    def get_stack(self, stack_id):
        """
        Find stack with stack_id and return it
        :param stack_id: String
        :return: Stack
        """
        return [item for item in self.stacks if item.stack_id == stack_id][0]

    def add_stack(self, stack):
        '''
        Add stack and store it into db
        :param stack: Stack
        :return
        '''
        self.stacks.append(stack)
        data_string = '\"' + stack.stack_id + '\", \"'
        for flavor in stack.flavors:
            data_string += flavor.name + ','
        data_string = data_string[:-1]
        data_string += '\"'
        self.db.write('Stack', 'stack_id, flavors', data_string)

    def delete_stack(self, stack_id):
        '''
        Delete all the resources allocated for this stack. Then delete the stack instance from data center
        :param stack_id: String
        :return stack_id: String

        '''
        try:
            stack = self.get_stack(stack_id)
            print(stack)
            for flavor in stack.flavors:
                #flavor = get_flavor(flavor['flavor'])
                host_id = flavor.host_id
                self.get_host(host_id).delete_flavor(flavor)
            del self.stacks[self.stacks.index(stack)]
            self.db.delete('Stack', 'stack_id=\"' + stack_id + '\"')
            return stack_id
        except IndexError:
            raise NoStackFoundException('Stack not found')

    def print_flavors(self):
        """
        Print all flavors
        :return:
        """
        for stack in self.stacks:
            print(stack.stack_id)
            print('---------------------------------------')
            print('flavor_name | host_name')
            for flavor in stack.flavors:
                print(flavor.name, flavor.host_id)
            print('---------------------------------------')

    def print_hosts(self):
        """
        Print all hosts resources
        :return:
        """
        for host in self.hosts:
            print(host.host_id)
            print(host.CPU, host.used_CPU)
            print(host.RAM, host.used_RAM)
            print(host.storage, host.used_storage)


