class WrongConfigurationFile(Exception):
    def __init__(self, message):
        self.message = message
        # Call the base class constructor with the parameters it needs
        super(WrongConfigurationFile, self).__init__(message)

    def get_mess(self):
        return self.message

class WrongBlockDeviceMappingFormat(Exception):
    def __init__(self, message):
        self.message = message
        # Call the base class constructor with the parameters it needs
        super(WrongBlockDeviceMappingFormat, self).__init__(message)

    def get_mess(self):
        return self.message

class NoHostFoundException(Exception):
    def __init__(self, message):
        self.message = message
        # Call the base class constructor with the parameters it needs
        super(NoHostFoundException, self).__init__(message)

    def get_mess(self):
        return self.message

class NoStackFoundException(Exception):
    def __init__(self, message):
        self.message = message
        # Call the base class constructor with the parameters it needs
        super(NoStackFoundException, self).__init__(message)

    def get_mess(self):
        return self.message