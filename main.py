from config import Configuration
import logging
import sys
import math
import subprocess
from datacenter import Datacenter
from ruamel.yaml.compat import StringIO as ruamelStringIO
from io import StringIO
from bottle import run, response, post, request, delete, get
from ruamel.yaml import YAML  # To create YAML File and Read YAML File
from pathlib import Path
from db import Database
from api import deploy_stack, undeploy_stack, print_snapshot
from heatclient import client
from heatclient import exc
from keystoneauth1 import loading, session
from translator.shell import TranslatorShell as TOSCATranslatorShell
from exception import WrongBlockDeviceMappingFormat, NoHostFoundException, NoStackFoundException
from sqlite3 import Error as sqliteError

# load configuration
conf = Configuration()
# initialize logging
conf.log_configuration()
logging.debug('Main started')
print("Debug mode is set to: ", conf.DEBUG_MODE)

try:
    db = Database('knowledge_base.db')
    loader = loading.get_plugin_loader('password')
    auth = loader.load_from_options(auth_url=conf.KEYSTONE_SERVER, username=conf.USER, password=conf.PASSWORD,
                                    user_domain_id=conf.USER_DOMAIN, project_id=conf.PROJECT_ID)
    sess = session.Session(auth=auth)
    heat = client.Client('1', session=sess)
    datacenter = Datacenter(db=db)
except sqliteError as e:
    print("Error connecting to database!")
    logging.exception(e)
    exit(2)
except Exception as e:
    print(e)
    logging.exception(e)
    response.status = 500
    exit(3)


@post('/flavor')
def insert_flavor():
    """
    Expose an API to insert a new flavor in the database. To use it reboot the orchestrator.
    This function receive a JSON that specify flavor features.
    :return: JSON
    """
    try:
        data = request.json
        to_write = '\"' + data['flavor'] + '\", \"' + str(data['CPU']) + '\", \"' + str(data['RAM']) + '\", \"' \
                   + str(data['storage']) + '\", \"' + data['host'] + '\"'
        db.write('Flavor', 'flavor, CPU, RAM, storage, host', to_write)
        return data
    except sqliteError as e:
        logging.exception(e)
        response.status = 400
        return "Error: %s" % e
    except Exception as e:
        print(e)
        logging.exception(e)
        response.status = 500
        return "An exception occurred, check the log file"


@post('/tag')
def insert_tag():
    """
        Expose an API to insert a new tag in the database. To use it reboot the orchestrator.
        This function receive a JSON that specify tag features.
        :return: JSON
        """
    try:
        data = request.json
        to_write = '\"' + str(data['tag']) + '\", \"' + data['flavor'] + '\"'
        db.write('Tag', 'tag, flavor', to_write)
        return data

    except sqliteError as e:
        logging.exception(e)
        response.status = 400
        return "Error: %s" % e
    except Exception as e:
        print(e)
        logging.exception(e)
        response.status = 500
        return "An exception occurred, check the log file"


@post('/host')
def insert_host():
    """
        Expose an API to insert a new host in the database. To use it reboot the orchestrator.
        This function receive a JSON that specify host features.
        :return: JSON
        """
    try:
        data = request.json
        to_write = '\"' + data['host'] + '\", \"' + str(data['CPU']) + '\", \"' + str(data['RAM']) + '\", \"' + str(
            data['storage']) + \
                   '\", \"' + str(data['used_CPU']) + '\", \"' + str(data['used_RAM']) + '\", \"' + str(
            data['used_storage']) + '\", \"' + \
                   data['power_array'] + '\"'
        db.write('Host', 'host, CPU, RAM, storage, used_CPU, used_RAM, used_storage, power_array', to_write)
        return data

    except sqliteError as e:
        logging.exception(e)
        response.status = 400
        return "Error: %s" % e
    except Exception as e:
        print(e)
        logging.exception(e)
        response.status = 500
        return "An exception occurred, check the log file"


@delete('/orchestrator/<stack_id>')
def delete_stack(stack_id):
    """
    Expose an API to delete deployed stack.
    :param stack_id: string
    :return: string
    """
    try:
        return undeploy_stack(conf, datacenter, heat, stack_id)
    except NoStackFoundException as e:
        logging.exception(e)
        response.status = 400
        return "Stack not found"
    except exc.HTTPException as e:
        print("HTTPException", e)
        logging.exception(e)
        response.status = 400
        return "Stack not found"
    except Exception as e:
        print(e)
        logging.exception(e)
        response.status = 500
        return "An exception occurred, check the log file"


@post('/orchestrator')
def insert_stack():
    """
    Expose an API to deploy a new stack.
    This function receive a stack as tosca template (version: tosca_simple_yaml_1_0)
    :return stack_id : string
    """
    try:
        return deploy_stack(conf, db, datacenter, heat)
    except NoHostFoundException as e:
        print(e)
        logging.exception(e)
        response.status = 500
        return e.get_mess()
    except WrongBlockDeviceMappingFormat as e:
        print(e)
        logging.exception(e)
        response.status = 400
        return e.get_mess()
    except exc.HTTPException as e:
        print(e)
        logging.exception(e)
        response.status = 400
        return e
    except IndexError as e:
        print(e)
        logging.exception(e)
        response.status = 403
        return 'Flavor not found'
    except Exception as e:
        print(e)
        logging.exception(e)
        response.status = 500
        return "An exception occurred, check the log file"


@get('/snapshot')
def get_snapshot():
    """
    Debug API: this function prints on console a brief snapshot of stacks and hosts
    :return:
    """
    print_snapshot(datacenter)
    return 

@get('/reset')
def reset():
    """
    Debug API: this function reset the entire orchestrator to a empty situation.
    It DON'T reset the openstack setup
    :return:
    """
    for host in datacenter.hosts:
        host.used_CPU=0
        host.used_RAM=0
        host.used_storage=0
        db.update('Host', 'used_CPU=\"0\", used_RAM=\"0\", used_storage=\"0\"','host=\"'+host.host_id+'\"')
    db.query('DELETE FROM Stack;')
    datacenter.stacks=[]

@post('/migrating')
def migrating():
    """
    Wrap CRIU api for container migration
    """
    print(request.query.source_ip, "___", request.query.container_name, "___", request.query.dest_ip)
    bashCommand = "lxc --version"
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    print(output.decode("utf-8"));

def main():
    try:
        run(host=conf.IP_ADDRESS, port=conf.PORT)

    except Exception as e:
        print(e)
        logging.exception(e)
        return -1


if __name__ == '__main__':
    main()