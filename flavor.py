
class Flavor:
    """
    Flavor class to manage flavor object
    """
    def __init__(self, name, CPU, RAM, storage, host_id):
        self.name = name
        self.CPU = CPU
        self.RAM = RAM
        self.storage = storage
        self.host_id = host_id