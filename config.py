import configparser
import logging
from exception import WrongConfigurationFile


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Configuration(object, metaclass=Singleton):
    def __init__(self):
        self.conf_file = "config.ini"
        self.log_init = False
        self.inizialize()

    def inizialize(self):
        config = configparser.RawConfigParser()
        base_folder = ""
        try:
            if base_folder == "":
                config.read(str(base_folder) + self.conf_file)
            else:
                config.read(str(base_folder) + '/' + self.conf_file)#TODO: only with unix system
            self._IP_ADDRESS = config.get('network', 'ip_address')
            self._PORT = config.get('network', 'port')
            # [log]
            self._LOG_FILE = config.get('log', 'log_file')
            self.__LOG_LEVEL = config.get('log', 'log_level')
            self.__APPEND_LOG = config.get('log', 'append_log')
            # [Openstack]
            self._KEYSTONE_SERVER = config.get('openstack', 'keystone_server')
            self._USER_DOMAIN = config.get('openstack', 'user_domain')
            self._USER = config.get('openstack', 'user')
            self._PASSWORD = config.get('openstack', 'password')
            self._PROJECT_ID = config.get('openstack', 'project_id')
            self._DEBUG_MODE = config.get('openstack', 'debug_mode')
        except Exception as ex:
            raise WrongConfigurationFile(str(ex))

    def log_configuration(self):
        if not self.log_init and not self.__APPEND_LOG:
            try:
                print("Trying to init log file, fill with appropriate call")
                #TODO os.remove(self.LOG_FILE)
            except OSError:
                pass
        log_format = '%(asctime)s.%(msecs)03d %(levelname)s %(message)s - %(filename)s:%(lineno)s'
        if self.__LOG_LEVEL == "DEBUG":
            log_level = logging.DEBUG
            requests_log = logging.getLogger("requests")
            requests_log.setLevel(logging.WARNING)
        elif self.__LOG_LEVEL == "INFO":
            log_level = logging.INFO
            requests_log = logging.getLogger("requests")
            requests_log.setLevel(logging.WARNING)
        elif self.__LOG_LEVEL == "WARNING":
            log_level = logging.WARNING
        else:
            log_level = logging.ERROR
        logging.basicConfig(filename=self.LOG_FILE, level=log_level, format=log_format, datefmt='%d/%m/%Y %H:%M:%S')
        logging.info("[CONFIG] Logging just started!")
        self.log_init = True

    @property
    def IP_ADDRESS(self):
        return self._IP_ADDRESS

    @property
    def PORT(self):
        return self._PORT

    @property
    def LOG_FILE(self):
        return self._LOG_FILE

    @property
    def KEYSTONE_SERVER(self):
        return self._KEYSTONE_SERVER

    @property
    def USER_DOMAIN(self):
        return self._USER_DOMAIN

    @property
    def PROJECT_ID(self):
        return self._PROJECT_ID

    @property
    def USER(self):
        return self._USER

    @property
    def PASSWORD(self):
        return self._PASSWORD

    @property
    def DEBUG_MODE(self):
        return self._DEBUG_MODE