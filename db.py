###########################################################################
#
## @file database.py
#
###########################################################################

import sqlite3
import logging


class Database:

    #######################################################################
    #
    #  The constructor of the Database class
    #
    #  The constructor can either be passed the name of the database to open
    #  or not, it is optional. The database can also be opened manually with
    #  the open() method or as a context manager.
    #
    #  @param name Optionally, the name of the database to open.
    #
    #  @see open()
    #
    #######################################################################

    def __init__(self, name=None):

        self.conn = None
        self.cursor = None

        if name:
            self.open(name)
            self.create_tables()

    #######################################################################
    #
    #  Opens a new database connection.
    #
    #  This function manually opens a new database connection. The database
    #  can also be opened in the constructor or as a context manager.
    #
    #  @param name The name of the database to open.
    #
    #  @see \__init\__()
    #
    #######################################################################

    def open(self, name):

            self.conn = sqlite3.connect(name)
            self.cursor = self.conn.cursor()

    #######################################################################
    #
    #  Check and create tables.
    #
    #  @see \__init\__()
    #
    #######################################################################

    def create_tables(self):

        self.query('''CREATE TABLE IF NOT EXISTS Flavor
                     (flavor text NOT NULL, CPU int NOT NULL, RAM int NOT NULL, storage int NOT NULL, host text NOT 
                     NULL, PRIMARY KEY (flavor), FOREIGN KEY (host) REFERENCES Host(host));''')
        self.query('''CREATE TABLE IF NOT EXISTS Tag
                     (tag int NOT NULL, flavor text NOT NULL, PRIMARY KEY (tag, flavor), FOREIGN KEY (flavor)
                     REFERENCES Flavor(flavor));''')
        self.query('''CREATE TABLE IF NOT EXISTS Host
                     (host text NOT NULL, CPU int NOT NULL, RAM int NOT NULL, storage int NOT NULL, used_CPU int 
                     NOT NULL, used_RAM int NOT NULL, used_storage int NOT NULL, power_array text NOT NULL, 
                     PRIMARY KEY (host));''')
        self.query('''CREATE TABLE IF NOT EXISTS Stack
                     (stack_id text NOT NULL, flavors text NOT NULL);''')


    #######################################################################
    #
    #  Function to close a datbase connection.
    #
    #  The database connection needs to be closed before you exit a program,
    #  otherwise changes might be lost. You can also manage the database
    #  connection as a context manager, then the closing is done for you. If
    #  you opened the database connection with the open() method or with the
    #  constructor ( \__init\__() ), you must close the connection with this
    #  method.
    #
    #  @see open()
    #
    #  @see \__init\__()
    #
    #######################################################################

    def close(self):

        if self.conn:
            self.conn.commit()
            self.cursor.close()
            self.conn.close()

    def __enter__(self):

        return self

    def __exit__(self, exc_type, exc_value, traceback):

        self.close()

    #######################################################################
    #
    #  Function to fetch/query data from a database.
    #
    #  This is the main function used to query a database for data.
    #
    #  @param table The name of the database's table to query from.
    #
    #  @param columns The string of columns, comma-separated, to fetch.
    #
    #  @param limit Optionally, a limit of items to fetch.
    #
    #######################################################################

    def get(self, table, columns, limit=None):

        query = "SELECT {0} from {1};".format(columns, table)
        self.cursor.execute(query)

        # fetch data
        rows = self.cursor.fetchall()

        return rows[len(rows) - limit if limit else 0:]

    #######################################################################
    #
    #  Function to fetch/query data from a database.
    #
    #  This is the main function used to query a database for data.
    #
    #  @param table The name of the database's table to query from.
    #
    #  @param columns The string of columns, comma-separated, to fetch.
    #
    #  @param limit Optionally, a limit of items to fetch.
    #
    #######################################################################

    def getWhere(self, table, columns, condition, limit=None):

        query = "SELECT {0} from {1} WHERE {2};".format(columns, table, condition)
        self.cursor.execute(query)

        # fetch data
        rows = self.cursor.fetchall()

        return rows[len(rows) - limit if limit else 0:]

    #######################################################################
    #
    #  Utilty function to get the last row of data from a database.
    #
    #  @param table The database's table from which to query.
    #
    #  @param columns The columns which to query.
    #
    #######################################################################

    def getLast(self, table, columns):

        return self.get(table, columns, limit=1)[0]

    #######################################################################
    #
    #  Utility function that converts a dataset into CSV format.
    #
    #  @param data The data, retrieved from the get() function.
    #
    #  @param fname The file name to store the data in.
    #
    #  @see get()
    #
    #######################################################################

    @staticmethod
    def toCSV(data, fname="output.csv"):

        with open(fname, 'a') as file:
            file.write(",".join([str(j) for i in data for j in i]))

    #######################################################################
    #
    #  Function to write data to the database.
    #
    #  The write() function inserts new data into a table of the database.
    #
    #  @param table The name of the database's table to write to.
    #
    #  @param columns The columns to insert into, as a comma-separated string.
    #
    #  @param data The new data to insert, as a comma-separated string.
    #
    #######################################################################

    def write(self, table, columns, data):

        query = "INSERT INTO {0} ({1}) VALUES ({2});".format(table, columns, data)

        self.cursor.execute(query)
        self.conn.commit()

     #######################################################################
    #
    #  Function to update data to the database.
    #
    #  The update() function updates data into a table of the database.
    #
    #  @param table The name of the database's table to update to.
    #
    #  @param columns The columns to ins into, as a comma-separated string.
    #
    #  @param data The new data to insert, as a comma-separated string.
    #
    #######################################################################

    def update(self, table, parameters, condition):

        query = "UPDATE {0} SET {1} WHERE {2};".format(table, parameters, condition)
        print(query)

        self.cursor.execute(query)
        self.conn.commit()

     #######################################################################
    #
    #  Function to delete data to the database.
    #
    #  The delete() function deletes data into a table of the database.
    #
    #  @param table The name of the database's table to update to.
    #
    #  @param columns The columns to ins into, as a comma-separated string.
    #
    #  @param data The new data to insert, as a comma-separated string.
    #
    #######################################################################

    def delete(self, table, condition):

        query = "DELETE FROM {0} WHERE {1};".format(table, condition)
        print(query)

        self.cursor.execute(query)
        self.conn.commit()

    #######################################################################
    #
    #  Function to query any other SQL statement.
    #
    #  This function is there in case you want to execute any other sql
    #  statement other than a write or get.
    #
    #  @param sql A valid SQL statement in string format.
    #
    #######################################################################

    def query(self, sql):
        self.cursor.execute(sql)
        self.conn.commit()

    #######################################################################
    #
    #  Utility function that summarizes a dataset.
    #
    #  This function takes a dataset, retrieved via the get() function, and
    #  returns only the maximum, minimum and average for each column.
    #
    #  @param rows The retrieved data.
    #
    #######################################################################

    @staticmethod
    def summary(rows):

        # split the rows into columns
        cols = [[r[c] for r in rows] for c in range(len(rows[0]))]

        # the time in terms of fractions of hours of how long ago
        # the sample was assumes the sampling period is 10 minutes
        t = lambda col: "{:.1f}".format((len(rows) - col) / 6.0)

        # return a tuple, consisting of tuples of the maximum,
        # the minimum and the average for each column and their
        # respective time (how long ago, in fractions of hours)
        # average has no time, of course
        ret = []

        for c in cols:
            hi = max(c)
            hi_t = t(c.index(hi))

            lo = min(c)
            lo_t = t(c.index(lo))

            avg = sum(c) / len(rows)

            ret.append(((hi, hi_t), (lo, lo_t), avg))

        return ret