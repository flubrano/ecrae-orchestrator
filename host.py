from flavor import Flavor

class Host:
    """
    Host class represents host in the orchestrator
    """

    def __init__(self, db, host_id, CPU, RAM, storage, used_CPU, used_RAM, used_storage, power_array):
        self.db = db
        self.host_id = host_id
        self.CPU = CPU
        self.RAM = RAM
        self.storage = storage
        self.used_CPU = used_CPU
        self.used_RAM = used_RAM
        self.used_storage = used_storage
        self.power_array = power_array

    def add_flavor(self, flavor):
        """
        Allocate the flavor resources in the selected host
        :param flavor: Flavor
        :return:
        """
        self.used_CPU += flavor.CPU
        self.used_RAM += flavor.RAM
        self.used_storage += flavor.storage
        self.db.update('Host', 'used_CPU=\"'+str(self.used_CPU)+'\", used_RAM=\"'+str(self.used_RAM)+'\", used_storage=\"'
            +str(self.used_storage)+'\"','host=\"'+self.host_id+'\"')

    def delete_flavor(self, flavor):
        """
        Free the flavor resources
        :param flavor: Flavor
        :return:
        """
        self.used_CPU -= flavor.CPU
        self.used_RAM -= flavor.RAM
        self.used_storage -= flavor.storage
        self.db.update('Host', 'used_CPU=\"'+str(self.used_CPU)+'\", used_RAM=\"'+str(self.used_RAM)+'\", used_storage=\"'
            +str(self.used_storage)+'\"','host=\"'+self.host_id+'\"')

    def is_suitable(self, flavor):
        """
        Check if the host has enough free resources to suite the flavor instance
        :param flavor: Flavor
        :return:
        """
        f_CPU = flavor.CPU
        f_RAM = flavor.RAM
        f_storage = flavor.storage
        if self.CPU >= (self.used_CPU + f_CPU) and self.RAM >= (self.used_RAM + f_RAM) and self.storage > (self.used_storage + f_storage):
            return True
        else:
            return False

