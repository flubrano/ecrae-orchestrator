# ECRAE Orchestrator

## Description
ECRAE Orchestrator is an orchestrator running on top of Openstack Heat. ECRAE gets through its HTTP interface (/orchestrator) a TOSCA template with custom field, finds a suitable host where deploy the vm or container filling the TOSCA with other informations and translates the TOSCA into Heat template.

## Installation
This module require the bottle framework to run the WebService and the ruamel.yaml library to parse YAML file.


Install [YAML library](http://yaml.readthedocs.io/en/latest/install.html) :

    pip install ruamel.yaml

Install [Bottle framework](https://bottlepy.org/docs/dev/):

    pip install bottle
Install [Tosca parser](https://docs.openstack.org/tosca-parser/latest/index.html)

    pip install tosca-parser
The heat-translator module is forked from official repository and modified for ECRAE orchestrator purpose, so it is imported from source in this repository
